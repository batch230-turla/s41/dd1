const Product = require("../models/product");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database.
*/

module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		pieces: req.body.pieces
	});

	if(req.userData.isAdmin){

		// Saves the created object to our database
		return newProduct.save()
		// Course creation successful
		.then(product => {
			console.log(product);
			res.send(true)
		})
		// Course creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else {
		// return res.send(false);
		return res.status(401).send("Gotcha!");
	};

};


const User = require("../models/User.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
		//
		})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}

		})
	}
//for postman s37 checkemail
		module.exports.checkEmailExist = (reqBody) =>{
			return User.find({email: reqBody.email}).then(result => {
				if(result.length > 0){
					return true;
				}
				else{
					return false;
				}
			})
		}
//for postman s37 login
		module.exports.loginUser = (reqBody)=>{
			return User.findOne({email: reqBody.email}).then(result =>{
				if (result == null){
					return false;
				}
				else{
					//compareSync is a bcrypt function to compare unhashed passsword to hash pasword
					const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password );
					//true of false

					if(isPasswordCorrect){
						//lets give the user a token to a access features
						return {access: auth.createAccessToken(result)};
					}
					else{
						//if the password does not match, else
						return false;
					}
				}
			})

		}



//--------------------------
	






// 		module.exports.getAllUserController = (req, res) => {
// 	User.find({})
// 	.then(result => res.send(result))
// 	.catch(error => res.send(error));
// }






// 		module.exports.loginDetails = (reqBody)=>{
// 			return User.findById(reqBody._id).then(result =>{
// 				if (result == null){
// 					return false;
// 				}
// 				else{
// 					//compareSync is a bcrypt function to compare unhashed passsword to hash pasword
// 					const isIdCorrect = bcrypt.compareSync(reqBody.Id, result.Id );
// 					//true of false

// 					if(isIdCorrect){
// 						//lets give the user a token to a access features
// 						return {access: auth.createAccessToken(result)};
// 					}
// 					else{
// 						//if the password does not match, else
// 						return false;
// 					}
// 				}
// 			})
// 		}




//38 Activity
// module.exports.getProfile =(reqBody)=>{
// 	return User.findById(reqBody._id).then((result, err)=> {
// 		if(err){
// 			return false;
// 		}
// 		else{
// 			result.password = "******";
// 			return result;
// 		}
// 	})
// }

//for get token s41 
module.exports.getProfile =(request,response)=>{
	//will contain your decoded token
	const userData =auth.decode(request.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result =>{
		result.passowrd = "******";
		response.send(result);
	})
}


//enroll feature

module.exports.enroll = async (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let courseName = await Course.findById(req.body.courseId).then(result => result.name);

	let data = {
		// User ID and email will be retrieved from the request header
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved form the request body
		courseId:req.body.courseId,
		// courseName value is retrieved using findById method.
		courseName: courseName
	}

	console.log(data);

		// a user is updated if we receive a "true" value
	let isUserUpdated = await User.findById(data.userId)
	.then(user =>{
		user.enrollments.push({
			courseId: data.courseId,
			courseName: data.courseName
		});

		// Save the updated user information in the database
		return user.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	// module.exports.getProfile =(reqBody)=>{
// 	return User.findById(reqBody._id).then((result, err)=> {
// 		if(err){
// 			return false;
// 		}
// 		else{
// 			result.password = "******";
// 			return result;
// 		}
// 	})
// }



	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{

		if(course.slots<=0){
			return console.log("Sorry no more slots lefts")
		}

		course.enrollees.push({
			userId: data.userId,
			email: data.email
		})

		
		//Mini activity 
		//[1] Create a condition that if the slot is already zero, no deduction of slot will happen
		//[2] it should have a message in the terminal that the slot is already zero
		//[3] else if the slot is negative value.


	
		
		
		// Minus the slots available by 1
		course.slots -= 1;
		
		
		return course.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;

		})
	})

	console.log(isCourseUpdated);

	// Condition will check if the both "user" and "course" document have been updated.
	// ternary operator
	(isUserUpdated && isCourseUpdated) ? res.send(true) : res.send(false)

}

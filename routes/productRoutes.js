const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth =require("../auth.js");

// 1. Refractor the "product" route to implement user authentication for the admin when creating a product.

//creating a product

//___________________________


router.post("/newProduct", auth.verify, productControllers.addProduct);


	

module.exports = router;